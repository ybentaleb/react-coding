# ReactJs Coding Test

## Requirements

Développer une application qui permet de récupérer les informations des films en utilisant OMDb API qui est bien Open source.

## Tasks

- Etant qu'utilisateur, je dois pouvoir consulter le détail d'un film en entrant son titre comme paramètre de recherche. [Details = (Title, Year, Languages, country)]
- Etant qu'utilisateur je veux pourvoir afficher la liste des films filtrée (type = movie) et que leurs titres contiennent la chaîne de caractère entrée en champ de recherche, en affichant les details mentionnées ci-dessus
- Etant qu'utilisateur je dois pourvoir voir le résultat paginé.

## Evaluation

- On s'intéresse plus de la qualité de code que les taches demandés, vaut mieux avoir un code bien structuré et qui respecte au maximum les bonnes pratiques et des taches non finalisées, qu'un code qu'on ne peut pas lire.
- Le code doit être versionné sur git, avec des commits regulierements.
- README qui accompagne le projet (argumentation de choix de libraire si nécessaire, les étapes pour lancer le projet)
- L'évaluation se fera sur la qualité et l'organisation de code, les tests unitaires, l'historique des commits. (en aucun cas, l'évaluation ne portera pas sur le nombre de taches finalisées)

## L'utilisation de OMDB API

- Commencer par recuperer votre API Key depuis
  `http://www.omdbapi.com/apikey.aspx`

- Ensuite vous pouvez fetcher les données comme démontré dans la documentation sur le lien `http://www.omdbapi.com/`

## Contact

En cas de blocage ou mal compréhension, n'hésitez pas de nous contacter:
<ybentaleb@octo.com> - <aahouzi@octo.com>
